Name: eggo
Version: 0.9.4
Release: 10
Summary: Eggo is a tool built to provide standard multi-ways for creating Kubernetes clusters.
License: Mulan PSL V2
URL: https://gitee.com/openeuler/eggo
Source0: https://gitee.com/openeuler/eggo/repository/archive/v%{version}.tar.gz

Patch0001:	0001-correct-docs-error.patch
Patch0002:	0002-disable-service-beforce-setup-service.patch
Patch0003:	0003-support-disable-rollback-for-deploy-cluster.patch
Patch0004:	0004-add-host-name-checker-add-bin-execute-permission.patch
Patch0005:	0005-wait-task-longer-and-modify-eggops-docs.patch
Patch0006:	0006-fix-eggo-deploy-bugs.patch  
Patch0007:	0007-non-root-user-deploy-bug-fixed.patch
Patch0008:	0008-fix-post-join-hook-do-not-run-bug.patch
Patch0009:	0009-add-spec-of-eggo-hook.patch
Patch0010:	0010-update-roadmap-of-eggo.patch
Patch0011:	0011-run-hook-with-envs-of-cluster-info.patch
Patch0012:	0012-add-deploy-problems-doc.patch
Patch0013:	0013-add-cleanup-last-step.patch
Patch0014:	0014-refactor-dependency.patch
Patch0015:	0015-delete-apiserver-kubelet-https-flag-and-add-lb-bind-.patch
Patch0016:	0016-add-vendor-LICENSE.patch
Patch0017:	0017-fix-Makefile-build-error.patch
Patch0018:	0018-implement-cmd-hooks.patch
Patch0019:	0019-add-design-of-eggoadm.patch
Patch0020:	0020-add-digitalSignature-for-certificates.patch
Patch0021:	0021-gitignore-style-global-ignore-IDE-and-gomod-vendor.patch
Patch0022:	0022-add-golangci-check.patch
Patch0023:	0023-add-golang-static-code-check.patch
Patch0024:	0024-modify-dependency-install-command.patch
Patch0025:	0025-fix-panic-in-os-user.Current.patch
Patch0026:	0026-fix-riscv64-support.patch

BuildRequires: make
BuildRequires: git
BuildRequires: golang >= 1.13

%description
Eggo is a tool built to provide standard multi-ways for creating Kubernetes clusters.

%define debug_package %{nil}

%prep
%autosetup -n eggo-v%{version} -Sgit -p1

%build
export GO111MODULE=off
rm -f go.mod go.sum
cp -rf vendor src
mkdir -p src/isula.org/eggo
cp -rf cmd pkg src/isula.org/eggo/
export GOPATH=$(pwd):$GOPATH
%ifarch riscv64
ARCH=riscv64 %{make_build} safe
%else
%{make_build} safe
%endif
strip bin/eggo

%install
rm -rf %{buildroot}
install -d %{buildroot}%{_bindir}
# install binary
install -p ./bin/eggo %{buildroot}%{_bindir}/eggo

%clean
rm -rf %{buildroot}
rm -rf src

%post

%files
# default perm for files and folder
%defattr(0640,root,root,0550)
%attr(551,root,root) %{_bindir}/eggo

%changelog
* Sun Apr 09 2023 misaka00251 <liuxin@iscas.ac.cn> - 0.9.4-10
- Type:feature
- CVE:NA
- SUG:NA
- DESC:Fix riscv64 support

* Tue Feb 07 2023 zhangxiaoyu <zhangxiaoyu58@huawei.com> - 0.9.4-9
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add omitted patch

* Mon Feb 06 2023 zhangxiaoyu <zhangxiaoyu58@huawei.com> - 0.9.4-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix panic in os user.Current

* Fri Feb 03 2023 zhangxiaoyu <zhangxiaoyu58@huawei.com> - 0.9.4-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:update from openeuler

* Mon Nov 28 2022 zhangxiaoyu <zhangxiaoyu58@huawei.com> - 0.9.4-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add yaml

* Wed Nov 16 2022 zhangxiaoyu <zhangxiaoyu58@huawei.com> - 0.9.4-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:update tar package from source

* Tue Sep 20 2022 zhangxiaoyu <zhangxiaoyu58@huawei.com> - 0.9.4-4
- Type:upgrade
- CVE:NA
- SUG:NA
- DESC:strip eggo

* Mon Sep 19 2022 zhangxiaoyu<zhangxiaoyu58@huawei.com> - 0.9.4-3
- Type:upgrade
- CVE:NA
- SUG:NA
- DESC:fix makefile build error

* Fri Nov 26 2021 zhangxiaoyu<zhangxiaoyu58@huawei.com> - 0.9.4-2
- Type:upgrade
- CVE:NA
- SUG:NA
- DESC:upgrade to v0.9.4-2

* Mon Sep 13 2021 zhangxiaoyu<zhangxiaoyu58@huawei.com> - 0.9.4-1
- Type:upgrade
- CVE:NA
- SUG:NA
- DESC:upgrade to v0.9.4-1

* Mon Sep 13 2021 zhangxiaoyu<zhangxiaoyu58@huawei.com> - 0.9.3-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:use local cert, add coredns checker, add list command, update cert of ca

* Tue Sep 07 2021 zhangxiaoyu<zhangxiaoyu58@huawei.com> - 0.9.3-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:eggo static compile

* Tue Sep 07 2021 zhangxiaoyu<zhangxiaoyu58@huawei.com> - 0.9.3-2
- Type:upgrade
- CVE:NA
- SUG:NA
- DESC:sync from openeuler

* Wed Sep 01 2021 zhangxiaoyu<zhangxiaoyu58@huawei.com> - 0.9.3-1
- Type:upgrade
- CVE:NA
- SUG:NA
- DESC:upgrade to v0.9.3-1

* Mon Aug 02 2021 zhangxiaoyu<zhangxiaoyu58@huawei.com> - 0.9.1-1
- Type:upgrade
- CVE:NA
- SUG:NA
- DESC:upgrade to v0.9.1-1

* Tue Jul 20 2021 zhangxiaoyu<zhangxiaoyu58@huawei.com> - 0.9.1-20210712.150722.gitddf3d38e
- Type:upgrade
- CVE:NA
- SUG:NA
- DESC:upgrade to v0.9.1

* Mon Jul 12 2021 wangfengtu<wangfengtu@huawei.com> - 0.9.0-20210712.150722.gitddf3d38e
- Type:add
- CVE:NA
- SUG:restart
- DESC:init repo
